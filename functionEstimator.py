import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.cross_validation import train_test_split

import numpy as np
from sklearn.decomposition import PCA

def makeData():

    X = np.random.randn(200, 500)
    y = np.random.randn(200)

    return(X, y)


def functionEstimator(X, y):

    #pca = PCA(n_components=2)
    #X = pca.fit_transform(X)
    model = LinearRegression()
    model.fit(X, y)

    def predictor(Xtest):
        #return model.predict(pca.transform(X))
        return model.predict(Xtest)

    return predictor

if __name__ == "__main__":

    X, y = makeData()
    Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, test_size=0.33, random_state=42)
    regressor = functionEstimator(Xtrain, ytrain)
    yhat = regressor(Xtest)

    print("SCORE: {score}".format(score=np.linalg.norm(yhat - ytest)))
